# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=lxd
pkgver=4.18
pkgrel=1
pkgdesc="a container hypervisor and a new user experience for LXC"
url="https://linuxcontainers.org/lxd/"
arch="all !mips !mips64"
license="Apache-2.0"
pkggroups="$pkgname"
depends="
	acl
	attr
	netcat-openbsd
	cgmanager
	squashfs-tools
	rsync
	dqlite
	shadow-uidmap
	lxc
	ip6tables
	dnsmasq
	ca-certificates
	tar
	iproute2
	xz
	"
makedepends="
	lxc-dev
	gettext-dev
	acl-dev
	acl-static
	tcl-dev
	libuv-dev
	eudev-dev
	libcap-dev
	linux-headers

	rsync
	go
	intltool

	libtool
	autoconf
	automake
	patchelf
	dqlite-dev
	dqlite-static
	lz4-dev
	sqlite-dev
	sqlite-static
	raft-dev
	raft-static
	gettext-static
	zlib-static
	libuv-static
	libseccomp-static
	libcap-static
	"
subpackages="
	$pkgname-scripts:scripts:noarch
	$pkgname-bash-completion
	$pkgname-openrc
	"
install="$pkgname.pre-install"
options="!check"
source="https://linuxcontainers.org/downloads/lxd/lxd-$pkgver.tar.gz
	$pkgname.confd
	$pkgname.initd
	"
ldpath="/usr/lib/lxd"
sonameprefix="$pkgname:"

build() {
	export GOPATH="$builddir/_dist"
	export GOFLAGS="$GOFLAGS -buildmode=pie -trimpath"
	export CGO_LDFLAGS="-lintl $LDFLAGS"
	export CGO_LDFLAGS_ALLOW="(-Wl,-wrap,pthread_create)|(-Wl,-z,now)"
	export GO111MODULE=on

	mkdir -p bin
	go build -v -tags "netgo" -ldflags '-extldflags "-static -lm -ldl -lz -lpthread -lz -lintl -lraft -ldqlite -luv -lseccomp -lcap"' -o bin/ ./lxd-p2c/...
	go build -v -tags "agent" -ldflags '-extldflags "-static -lm -ldl -lz -lpthread -lz -lintl -lraft -ldqlite -luv -lseccomp -lcap"' -o bin/ ./lxd-agent/...

	for tool in fuidshift lxc lxc-to-lxd lxd lxd-benchmark; do
		go build -v -tags "libsqlite3" -o bin/ ./$tool/...
	done
}

package() {
	for tool in lxc fuidshift lxc-to-lxd lxd lxd-benchmark lxd-p2c lxd-agent; do
		install -p -Dm755 "bin/$tool" "$pkgdir/usr/bin/$tool"
	done
	install -Dm755 bin/$pkgname "$pkgdir"/usr/sbin/$pkgname
	install -Dm755 bin/lxc "$pkgdir"/usr/bin/lxc

	patchelf --set-rpath "/usr/lib/lxd" "$pkgdir/usr/sbin/lxd"

	install -Dm755 "$srcdir"/lxd.initd \
		"$pkgdir"/etc/init.d/lxd
	install -Dm644 "$srcdir"/lxd.confd \
		"$pkgdir"/etc/conf.d/lxd

	mkdir -p "$pkgdir"/var/lib/lxd
	chmod 755 "$pkgdir"/var/lib/lxd
	chgrp $pkggroups "$pkgdir"/var/lib/lxd

	mkdir -p "$pkgdir/usr/share/doc/$pkgname"
		cat > "$pkgdir"/usr/share/doc/$pkgname/README.alpine <<EOF
-----------------------------------
Be sure to add your local user to the lxd group.
EOF
}

bashcomp() {
	depends="bash"
	pkgdesc="Bash completions for $pkgname"
	install_if="$pkgname=$pkgver-r$pkgrel bash-completion"

	cd $builddir
	mkdir -p "$subpkgdir"/usr/share/bash-completion/completions
	cp scripts/bash/lxd-client "$subpkgdir"/usr/share/bash-completion/completions/lxd-client
}

scripts() {
	pkgdesc="LXD scripts"
	depends="$pkgname py3-lxc jq"

	cd $builddir
	export GOPATH="$builddir/_dist"
	install -Dm755 scripts/empty-lxd.sh "$subpkgdir"/usr/bin/empty-lxd.sh
}

sha512sums="
b3bb2987e3351d55fba4c09d3c462e45025dee3482f51035889cf4d9354184646742e7afe3cd00ba34f2518e23f817b520040f4c94abeaf844753fac58a4f0ca  lxd-4.18.tar.gz
5af20408f4bbbcd20b60c610c26d9aa52798d4f970ef649ea1d3b14bdbb0ee87ab52166551a4fcf724a6641d4afdcdb07ef4d6da4a0b49448d2673e27b1b2f40  lxd.confd
94de0c0d5ab63463a929a4151359950b1117d0ada5ccf0944311cc70c6b6d4c437ccb4158734ab35db67bfb4abc437074c3f3515be4531f63adc74da21fefb5b  lxd.initd
"
