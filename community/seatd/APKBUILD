# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=seatd
pkgver=0.6.0
pkgrel=0
pkgdesc="Minimal seat management daemon"
url="https://sr.ht/~kennylevinsen/seatd/"
arch="all !ppc64le !mips64" # build failure
license="MIT"
options="suid"
pkggroups="seat"
install="$pkgname.pre-install"
makedepends="meson scdoc elogind-dev linux-headers"
subpackages="libseat:libs libseat-dev $pkgname-doc $pkgname-openrc"
source="
	$pkgname-$pkgver.tar.gz::https://git.sr.ht/~kennylevinsen/seatd/archive/$pkgver.tar.gz
	seatd.initd
	0001-man-Add-seatd-launch-1-to-SEE-ALSO-of-seatd-1.patch
	0002-seatd-launch-respect-PATH-when-looking-for-command.patch
	0003-seatd-launch-Use-optind-to-find-the-command.patch
	0004-man-seatd-launch-Make-mssage-about-root-clearer.patch
	"

build() {
	abuild-meson \
		-Dlibseat-logind=elogind \
		-Dman-pages=enabled \
		. output

	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild -v -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
	chmod u+s "$pkgdir"/usr/bin/seatd-launch

	install -Dm755 "$srcdir"/seatd.initd "$pkgdir"/etc/init.d/seatd
}

libs() {
	default_libs
	pkgdesc="Universal seat management library"
}

dev() {
	default_dev
	pkgdesc="Universal seat management library (development files)"
}

sha512sums="
9b0b67a028cf9bba63a6c4630c66b21dd2cb87de0d290bfc51245727cda72964fbd0ffee709eded1e81c28f403db6bae8926a257fdd9267e154fbe8697d22a2b  seatd-0.6.0.tar.gz
425e0249d1328ccef6502b1e0b8c8277bd3905fcccdabde0e497189db449b910d0b1956cf6e7c7a24991f30b2f41af8ffde490342c53ba161b26525320ae9607  seatd.initd
a3da796ab124e98ff6e2d120aacc0cc8d7b65b6d7690c5d105ff22a390d1b6729e4f8c2bcb932daf2f962ba38639273e2340b8950e52a95331e3f10e04069d8a  0001-man-Add-seatd-launch-1-to-SEE-ALSO-of-seatd-1.patch
67d42ddcde06da8b75478c4c1599c4bac2c5390bd2a206cb1cd6f2b31880fc9634ac3202f798e16768ad78a590b6cc7120efdbba797a5dffbc9d88c68903ffa4  0002-seatd-launch-respect-PATH-when-looking-for-command.patch
db3ef3e645936c090ea622fed9f4dcf42fe0662fcabfe2fa6612ee6aa91dc5f470909c0b16e9ade2e6e9a8af62ffbd5678377dc240c0d5c1438550247594a62b  0003-seatd-launch-Use-optind-to-find-the-command.patch
7f9c59868e1a664c7086f362a49597cb9d46e842ce5eacf49cfc3813e522f01dd5a3e96b36e3fee085bac10619c7676b56a6979dca87533927de83bac9817de9  0004-man-seatd-launch-Make-mssage-about-root-clearer.patch
"
