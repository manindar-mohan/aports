# Contributor: Tuan Hoang <tmhoang@linux.ibm.com>
# Maintainer: Tuan Hoang <tmhoang@linux.ibm.com>
pkgname=yq
pkgver=4.12.1
pkgrel=1
pkgdesc="portable command-line YAML processor written in Go"
url="https://github.com/mikefarah/yq"
# riscv64 blocked by govendor
arch="all !riscv64"
license="MIT"
makedepends="go govendor"
options="chmod-clean"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/mikefarah/yq/archive/v$pkgver.tar.gz"

build() {
	GOPATH="$srcdir" go build -v
}

check() {
	GOPATH="$srcdir" go test
}

package() {
	install -Dm755 yq "$pkgdir"/usr/bin/yq
	for file in LICENSE README.md; do
		install -Dm644 $file "$pkgdir"/usr/share/doc/$pkgname/$file
	done

	mkdir -p "$pkgdir"/usr/share/bash-completion/completions \
			"$pkgdir"/usr/share/zsh/site-functions \
			"$pkgdir"/usr/share/fish/completions
	"$pkgdir"/usr/bin/yq shell-completion bash \
		> "$pkgdir"/usr/share/bash-completion/completions/yq.bash
	"$pkgdir"/usr/bin/yq shell-completion zsh \
		> "$pkgdir"/usr/share/zsh/site-functions/_yq
	"$pkgdir"/usr/bin/yq shell-completion fish \
		> "$pkgdir"/usr/share/fish/completions/yq.fish
}

sha512sums="
69e0a03aa5186c8c182b2c9355e515074ae66cbb53c019d3ec0eb976e46d0f2162d5599caeee50654b1ed649c2729ce647caea497e05605b07d5062935cad457  yq-4.12.1.tar.gz
"
