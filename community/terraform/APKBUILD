# Contributor: Thomas Boerger <thomas@webhippie.de>
# Contributor: Gennady Feldman <gena01@gmail.com>
# Contributor: Sergii Sadovyi <serg.sadovoi@gmail.com>
# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Thomas Boerger <thomas@webhippie.de>
pkgname=terraform
pkgver=1.0.6
pkgrel=1
pkgdesc="Building, changing and combining infrastructure safely and efficiently"
url="https://www.terraform.io/"
arch="all"
license="MPL-2.0"
makedepends="go"
source="$pkgname-$pkgver.tar.gz::https://github.com/hashicorp/terraform/archive/v$pkgver.tar.gz"
builddir="$srcdir/src/github.com/hashicorp/$pkgname"
options="chmod-clean"

prepare() {
	mkdir -p ${builddir%/*}
	mv $srcdir/$pkgname-$pkgver "$builddir"/
	default_prepare
}

export GOPATH="$srcdir"

build() {
	go build -v -o bin/$pkgname \
		-mod=readonly -ldflags "-X main.GitCommit=v$pkgver -X github.com/hashicorp/terraform/version.Prerelease= -s -w"
}

check() {
	case "$CARCH" in
		arm*|x86)
			go list -mod=readonly . | xargs -t -n4 \
			go test -mod=readonly -timeout=2m -parallel=4
			;;
		*) go test ./... ;;
	esac
	bin/$pkgname -v
}

package() {
	install -Dm755 "$builddir"/bin/$pkgname "$pkgdir"/usr/bin/$pkgname
}

sha512sums="
765e244f7fbb565f350543cdbe62469e8d2248a28c9c916fd5c34cbcf5a79b06c7df068221828daafd8a1bf4a3869d4772f662f364bc6410d91471ede2eefd5b  terraform-1.0.6.tar.gz
"
